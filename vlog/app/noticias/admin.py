# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from app.noticias.models import Noticia,Tematica
# Register your models here.
admin.site.register(Noticia)
admin.site.register(Tematica)