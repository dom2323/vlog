from django.conf.urls import url,include
from app.noticias.views import Listado_Noticias
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    url(r'^listado_noticias$', Listado_Noticias.as_view(), name='listado_noticias'),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    