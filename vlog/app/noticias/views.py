# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.views.generic import ListView
from app.noticias.models import Noticia,Tematica



class Listado_Noticias(ListView):
	ordering = ['id']   
	model = Noticia
	template_name = 'index_noticias.html'

# Create your views here.
def index_noticias(request):#con request  se le indica que reciba la peticion
	return render(request, 'index_noticias.html')

