# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from django.db import models
from django.utils import timezone
# Create your models here.
import os

class Tematica(models.Model):
    nombre_tematica = models.CharField(max_length=30)
    def __unicode__(self):
		return '{}'.format(self.nombre_tematica)

def get_image_path(instance, filename):
    return os.path.join('media', str(instance.id), filename)


class Noticia(models.Model):
    autor = models.ForeignKey('auth.User', on_delete=models.CASCADE)#como el usuario que piblica 
    #la noticia
    titulo = models.CharField(max_length=200)
    imagen = models.ImageField(upload_to=get_image_path, blank=True, null=True)
    texto = models.TextField()
    fecha_creado = models.DateTimeField(default=timezone.now)
    fecha_publicado = models.DateTimeField(blank=True, null=True)
    tematica = models.ForeignKey(Tematica,null=True, blank=True)#una noticia puede tener mas de 
	#una tematica, relacion de uno a muchos
    def __unicode__(self):
		return '{}'.format(self.titulo)

