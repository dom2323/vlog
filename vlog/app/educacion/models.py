# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.
class Educacion(models.Model):
    nombre_institucion = models.CharField(max_length=30,blank=True, null=True)
    temas_impartido = models.CharField(max_length=30,blank=True, null=True)
    fecha_de_seminario = models.DateTimeField(auto_now_add=False)
    def __unicode__(self):
		return '{}'.format(self.nombre_institucion)
